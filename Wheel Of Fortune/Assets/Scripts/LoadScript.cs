﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[RequireComponent(typeof(References))]
public class LoadScript : MonoBehaviour {

  TextAsset textFile;
  TextReader reader;

  public List<string> sentences = new List<string>();
  public List<string> clues = new List<string>();

  private void Start()
  {
    string lineOfText;
    int lineNumber = 0;

    textFile = (TextAsset)Resources.Load("embedded", typeof(TextAsset));
    reader = new StringReader(textFile.text);

    while ((lineOfText = reader.ReadLine()) != null)
    {
      if (lineNumber % 2 == 0)
      {
        sentences.Add(lineOfText);
      }
      else
      {
        clues.Add(lineOfText);
      }

      lineNumber++;
    }

    SendMessage("Gather");
  }

}
